var x = 150,
    y = 80,

    vx = 0,
    mass = [],
    appmass = [],
    cordx = [],
    cordy = [],
    headx =50,
    heady =50,
    tail = 3,
    sw = 3,
    sh = 3,
    aw = 5,
    ah = 5,
    start = 0,
    keyControl = 5,
    appControl = 0,
    vy = 0;

var canvas = document.getElementById("canvas");
var ctx = canvas.getContext('2d');
//var apple = canvas.getContext('2d');

function main() {


    ctx.fillStyle = 'black';
    ctx.fillRect(0, 0, canvas.width, canvas.height);


    x = x + vx;
    y = y + vy;

    mass.push({x: x, y: y, color: ctx.fillStyle});
    if( mass.length > tail ){
        mass.shift();
    }

    ctx.fillStyle = 'Yellow';
    for( let i = 0; i < mass.length; i++ )
    {
        mass[i].color = 'Yellow';
        ctx.fillStyle = mass[i].color;
        ctx.fillRect(mass[i].x, mass[i].y, sw, sh);
        // console.log('--------------');
        // console.log(mass[i].x);
        // console.log(mass[i].y);
    }


    headx=headx+vx;
    heady=heady+vy;


    //console.log(mass.length);
    var l=mass.length-1;
    cordx[l]=mass[l].x;
    cordy[l]=mass[l].y;

    //kill myself
    for( let i = 3; i < mass.length-1; i++ )
    {
        if( cordx[l]== mass[i].x && cordy[l]== mass[i].y) //&& mass[l].x == 0 && mass[l].x == 300)
        {
            tail=sh;
            console.log(mass[i].x);
            alert('YOU ARE DEAD. PRESS "OK" FOR CONTINUE');
        }


    }
    for( let i = 0; i < mass.length-1; i++ )
    {
        if( cordx[l] <= 0 || cordx[l] >= 310 || cordy[l] <=0 || cordy[l] >= 150)//&& mass[l].x == 0 && mass[l].x == 300)
        {
            tail=sh;
            alert('YOU ARE DEAD. PRESS "F5" FOR RESTART');
        }

    }

    //apple spawn
    if (appControl==0) {
        var apple_x=getRandomInt(1,canvas.width-aw);
        var apple_y=getRandomInt(1,canvas.height-ah);
        appmass.push({x: apple_x, y: apple_y, color: 'red'});
        appControl=1;
    }
    ctx.fillStyle = 'red';
    for( let i = 0; i < 1; i++ )
    {
        appmass[i].color = 'red';
        ctx.fillStyle = appmass[i].color;
        ctx.fillRect(appmass[i].x, appmass[i].y, aw, ah);

    }


    //console.log(appmass)



    //eat apple
    if((appmass[0].x-aw/2 <= cordx[l] && cordx[l] <= appmass[0].x+aw/2) && (appmass[0].y-ah/2 <= cordy[l] && cordy[l] <= appmass[0].y+ah/2)){
        tail=tail+sh*3;
        appControl=0;
        appmass.shift();

    }


}


function control(e){
    start=1;
    console.log('started')
    if (e.keyCode == 37 && keyControl!==3) {
        console.log('left')
        vx = -1;
        vy = 0;
        keyControl = 1;
        // x = x + vx;
        // y = y + vy;
        // ctx.fillRect(x, y, 5, 5);
    }
    else if (e.keyCode == 38 && keyControl!==4){
        console.log('up')
        vx = 0;
        vy = -1;
        keyControl = 2;
        // x = x + vx;
        // y = y + vy;
    }
    else if (e.keyCode == 39 && keyControl!==1){
        console.log('right')
        vx = 1;
        vy = 0;
        keyControl = 3;
        // x = x + vx;
        // y = y + vy;
    }
    else if (e.keyCode == 40 && keyControl!==2){
        console.log('down')
        vx = 0;
        vy = 1;
        keyControl = 4;
        // x = x + vx;
        // y = y + vy;
    }
}

function getRandomInt(min, max)
{
    return Math.floor(Math.random() * (max - min + 1)) + min;
}



window.onload = function() {
    document.addEventListener("keydown", control);
    setInterval(main, 50);
};

